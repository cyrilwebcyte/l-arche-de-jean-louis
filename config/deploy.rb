require "bundler/vlad"
require 'vlad/maintenance'
# For deploy make rake of the server at the same vesion of the locale

set :application, "l-arche-de-jean-louis"
set :user, "cyril"
set :server, "vks10391.ip-37-59-125.eu"

set :repository,  "git://vks10391.ip-37-59-125.eu/larchedejeanlouis.git"
set :deploy_to, "/home/#{user}/web/rails/#{application}"
set :domain, "#{user}@#{server}"

namespace :vlad do
  remote_task :bundle do
    run "cd #{release_path} && bundle install --deployment --without development:test"
  end

  task :update_symlinks => [:bundle, :migrate]

  desc "Full deployment cycle"
  task :deploy => %w[vlad:update vlad:start_app vlad:assets:precompile vlad:cleanup]
end