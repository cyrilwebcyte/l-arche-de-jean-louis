Larchedejeanlouis::Application.routes.draw do
  resources :movies, :path => "les-videos"

  devise_for :admins do
    get "/login" => "devise/sessions#new"
    get "/logout" => "devise/sessions#destroy"
  end

  resources :thanks, :path => "remerciements"

  resources :messages, :path => "livre-or"

  resources :pictures, :path => "les-photos"

  root :to => "static_page#index"
  match "/societe" => "static_page#company", :as => :company
  match "/services" => "static_page#services", :as => :services
  match "/services/visite-a-domicile" => "static_page#home_visit", :as => :home_visit
  match "/services/taxi-animalier" => "static_page#pet_taxi", :as => :pet_taxi
  match "/services/promenade-canine" => "static_page#dog_walk", :as => :dog_walk
  match "/services/pension-canine" => "static_page#dog_sitting", :as => :dog_sitting
  match "/services/educateur-comportementaliste" => "static_page#dog_trainer", :as => :dog_trainer
  match "/contact" => "static_page#contact", :as => :contact
  match "/mentions-legales" => "static_page#legal_notice", :as => :legal_notice
  match "/sitemap.xml" => "static_page#sitemap"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "welcome#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
