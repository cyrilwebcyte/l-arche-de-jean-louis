# encoding: utf-8

##
# $ backup perform -t my_backup [-c <path_to_configuration_file>]

Backup::Model.new(:backup, "Backup complet du site de l'arche de jean louis") do

  ##
  # AJL [Archive]
  #
  archive :AJL do |archive|
    archive.add 'public/system/messages'
    archive.add 'public/system/movies'
    archive.add 'public/system/pictures'
  end

  ##
  # MySQL [Database]
  #
  database MySQL do |db|
    db.name               = "larche_de_jl"
    db.username           = "root"
    db.password           = "gc910358"
    db.host               = "localhost"
    db.port               = 3306
    db.socket             = "/var/run/mysqld/mysqld.sock"
  end

  ##
  # Gzip [Compressor]
  #
  compress_with Gzip do |compression|
    compression.best = true
    compression.fast = false
  end

  store_with Local do |local|
    local.path = '~/web/rails/backups/l-arche-de-jean-louis'
    local.keep = 3
  end

  ##
  # Mail [Notifier]
  #
  # The default delivery method for Mail Notifiers is 'SMTP'.
  # See the Wiki for other delivery options.
  # https://github.com/meskyanichi/backup/wiki/Notifiers
  #
  notify_by Mail do |mail|
    mail.on_success           = false
    mail.on_failure           = true

    mail.from                 = 'cyril.webcyte@gmail.com'
    mail.to                   = 'cyril.webcyte+l-arche-de-jean-louis@gmail.com'
    mail.address              = 'smtp.gmail.com'
    mail.port                 = 587
    mail.domain               = 'vps11474.ovh.net'
    mail.user_name            = 'cyril.webcyte@gmail.com'
    mail.password             = 'gc910358'
    mail.authentication       = 'plain'
    mail.enable_starttls_auto = true
  end

end

