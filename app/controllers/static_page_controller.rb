# coding: utf-8

class StaticPageController < ApplicationController

  def index
    pictures = Picture.last(9)
    @message = Message.last

    # Just keep essential informations
    @pictures = {}
    pictures.each_with_index do |picture, index|
      @pictures[index] = {"id" => picture.id, "title" => picture.title, "url" => picture.image.url(:rounded)};
    end
  end

  def contact
    if params[:contact_form]
      @contact_form = ContactForm.new params[:contact_form]
      if @contact_form.valid?
        Notifier.contact(@contact_form).deliver
        flash.now[:contact_form_success] = "Email envoyé avec succès"
        @contact_form = ContactForm.new
      end
    else
      @contact_form = ContactForm.new
    end
  end

  def sitemap
    @pages = [root_url, company_url, contact_url, legal_notice_url, pictures_url, movies_url, thanks_url, messages_url, new_messages_url]
    @pages.push(services_url, home_visit_url, pet_taxi_url, dog_walk_url, dog_sitting_url, dog_trainer_url)
    render :layout => false
  end
end
