class PicturesController < ApplicationController
  before_filter :authenticate_admin!, :except => "index"
  
  # GET /pictures
  # GET /pictures.xml
  def index
    @q = Picture.search(params[:q])
    @pictures = @q.result.order("id DESC").page(params[:page]).per(Picture::PER_PAGE)
  end

  # GET /pictures/new
  # GET /pictures/new.xml
  def new
    @picture = Picture.new
  end

  # GET /pictures/1/edit
  def edit
    @picture = Picture.find(params[:id])
  end

  # POST /pictures
  # POST /pictures.xml
  def create
    @picture = Picture.new(params[:picture])

    
    if @picture.save
      redirect_to(pictures_path, :notice => 'Picture was successfully created.')
    else
      render :action => "new"
    end
  end

  # PUT /pictures/1
  # PUT /pictures/1.xml
  def update
    @picture = Picture.find(params[:id])

    if @picture.update_attributes(params[:picture])
      redirect_to(pictures_path(:page => @picture.page), :notice => 'Picture was successfully updated.')
    else
      render :action => "edit"
    end
  end

  # DELETE /pictures/1
  # DELETE /pictures/1.xml
  def destroy
    @picture = Picture.find(params[:id])
    page = @picture.page
    @picture.destroy

    # test si il y a encore des item sur la page de l'item supprime
    pictures = Picture.order("created_at DESC").page(page).per(Picture::PER_PAGE)
    page -= 1 if pictures.count == 0

    redirect_to(pictures_url(:page => page))
  end
end
