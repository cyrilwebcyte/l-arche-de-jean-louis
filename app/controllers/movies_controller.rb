class MoviesController < ApplicationController
  before_filter :authenticate_admin!, :except => "index"

  # GET /movies
  # GET /movies.xml
  def index
    @movies = Movie.all.reverse
  end

  # GET /movies/new
  # GET /movies/new.xml
  def new
    @movie = Movie.new
  end

  # GET /movies/1/edit
  def edit
    @movie = Movie.find(params[:id])
  end

  # POST /movies
  # POST /movies.xml
  def create
    @movie = Movie.new(params[:movie])

    if @movie.save
      redirect_to(movies_path(:anchor => "movie_#{@movie.id}"), :notice => 'Movie was successfully created.')
    else
      render :action => "new"
    end
  end

  # PUT /movies/1
  # PUT /movies/1.xml
  def update
    @movie = Movie.find(params[:id])

    if @movie.update_attributes(params[:movie])
      redirect_to(movies_path(:anchor => "movie_#{@movie.id}"), :notice => 'Movie was successfully updated.')
    else
      render :action => "edit"
    end
  end

  # DELETE /movies/1
  # DELETE /movies/1.xml
  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    
    redirect_to(movies_url)
  end
end
