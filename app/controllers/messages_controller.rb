class MessagesController < ApplicationController
  before_filter :authenticate_admin!, :except => [:index, :new, :create]
  
  # GET /messages
  # GET /messages.xml
  def index
    @messages = Message.order("created_at DESC").page(params[:page]).per(Message::PER_PAGE)
  end

  # GET /messages/new
  # GET /messages/new.xml
  def new
    @message = Message.new
  end

  # GET /messages/1/edit
  def edit
    @message = Message.find(params[:id])
  end

  # POST /messages
  # POST /messages.xml
  def create
    @message = Message.new(params[:message])

    if @message.save
      redirect_to(messages_path, :notice => 'Message was successfully created.')
    else
      render :action => "new"
    end
  end

  # PUT /messages/1
  # PUT /messages/1.xml
  def update
    @message = Message.find(params[:id])

    if @message.update_attributes(params[:message])
      redirect_to(messages_path(:page => @message.page), :notice => 'Message was successfully updated.')
    else
      format.html render :action => "edit"
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.xml
  def destroy
    @message = Message.find(params[:id])
    page = @message.page
    @message.destroy

    # test si il y a encore des item sur la page de l'item supprime
    messages = Message.order("created_at DESC").page(page).per(Message::PER_PAGE)
    page -= 1 if messages.count == 0

    redirect_to(messages_url(:page => page))
  end
end
