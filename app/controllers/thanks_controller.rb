class ThanksController < ApplicationController
  before_filter :authenticate_admin!, :except => "index"

  # GET /thanks
  # GET /thanks.xml
  def index
    @thanks = Thank.all
  end

  # GET /thanks/new
  # GET /thanks/new.xml
  def new
    @thank = Thank.new
  end

  # GET /thanks/1/edit
  def edit
    @thank = Thank.find(params[:id])
  end

  # POST /thanks
  # POST /thanks.xml
  def create
    @thank = Thank.new(params[:thank])
 
    if @thank.save
      redirect_to(thanks_path, :anchor => "thank_#{@thank.id}", :notice => 'Thank was successfully created.')
    else
      render :action => "new"
    end
  end

  # PUT /thanks/1
  # PUT /thanks/1.xml
  def update
    @thank = Thank.find(params[:id])

    if @thank.update_attributes(params[:thank])
      redirect_to(thanks_path(:anchor => "thank_#{@thank.id}") , :notice => 'Thank was successfully updated.')
    else
      render :action => "edit"
    end
  end

  # DELETE /thanks/1
  # DELETE /thanks/1.xml
  def destroy
    @thank = Thank.find(params[:id])
    @thank.destroy

    redirect_to(thanks_url)
  end
end
