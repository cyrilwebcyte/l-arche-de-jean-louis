class Notifier < ActionMailer::Base
  default :from => "contact@larchedejeanlouis.com"
  
  def contact(contact_form)
    @contact_form = contact_form
    mail(:to => "contact@larchedejeanlouis.com",
         :subject => "[Site l'arche de Lean Louis] #{@contact_form.subject}")
  end
end
