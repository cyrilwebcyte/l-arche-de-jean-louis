module ApplicationHelper
  def form_errors!(form)
    format_errors(form)
  end

  def format_errors(object)
    return "" if object.errors.empty?

    messages = object.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = "#{pluralize(object.errors.count, "erreur")}"

    html = <<-HTML
<div id="error_explanation">
<h3>#{sentence}</h3>
<ul>#{messages}</ul>
</div>
    HTML

    html.html_safe
  end
end
