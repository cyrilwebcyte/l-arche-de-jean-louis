class ContactForm
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include ActiveModel::Translation

  attr_accessor :email, :subject, :message, :verification, :attributes

  validates :subject, :presence => true
  validates :email, :format => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  validates :message, :presence => true
  validates :verification, :length => {:message => :presence, :within => 0..0 }

  def initialize(attributes = {})
    @attributes = attributes
    @attributes.each do |key, value|
      send("#{key}=", value) if respond_to? "#{key}="
    end
  end

  def persisted?
    false
  end
end