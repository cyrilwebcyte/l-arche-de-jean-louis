class Message < ActiveRecord::Base
  PER_PAGE = 5
  
  has_attached_file :image,
    :styles => { :small => "76x73#" },
    :url => "/system/:class/image/:id/:style_:basename.:extension",
    :path => ":rails_root/public/system/:class/image/:id/:style_:basename.:extension",
    :default_url => "/:class/default.png"

  attr_accessor :verification, :attributes

  validates :name, :length => { :within => 3..20 }
  validates :message, :length => { :within => 10..1000 }
  validates :verification, :length => {:message => :presence, :within => 0..0 }, :on => :create

  def page()
    position = Message.where("created_at >= ?", self.created_at).count
    (position.to_f/PER_PAGE).ceil
  end

end
