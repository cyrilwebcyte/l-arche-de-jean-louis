class Thank < ActiveRecord::Base
  has_attached_file :picture,
    :styles => { :small => "76x73#" },
    :url => "/system/:class/picture/:id/:style_:basename.:extension",
    :path => ":rails_root/public/system/:class/picture/:id/:style_:basename.:extension",
    :default_url => "/:class/default.png"
end
