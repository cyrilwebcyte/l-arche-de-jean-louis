class Movie < ActiveRecord::Base
  has_attached_file :movie,
    :url => "/system/:class/movie/:id/:style_:basename.:extension",
    :path => ":rails_root/public/system/:class/movie/:id/:style_:basename.:extension"

  has_attached_file :movie_ogg,
    :url => "/system/:class/movie/:id/:style_:basename.:extension",
    :path => ":rails_root/public/system/:class/movie/:id/:style_:basename.:extension"

  def mp4_url
    movie.url()
  end

  def ogg_url()
    movie_ogg.url()
  end
end
