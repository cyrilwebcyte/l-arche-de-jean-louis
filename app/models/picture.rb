class Picture < ActiveRecord::Base
  PER_PAGE = 9

  has_attached_file :image,
    :styles => {
      :rounded => ["144x144#", :png],
      :small => "200x150>",
      :large => "1000x750>"
    },
    :convert_options => {:rounded => Proc.new{self.convert_options}},
    :url => "/system/:class/image/:id/:style_:basename.:extension",
    :path => ":rails_root/public/system/:class/image/:id/:style_:basename.:extension"


  def self.convert_options
    trans = ""
    px = 70
    trans << " \\( +clone  -threshold -1 "
    trans << "-draw 'fill black polygon 0,0 0,#{px} #{px},0 fill white circle #{px},#{px} #{px},0' "
    trans << "\\( +clone -flip \\) -compose Multiply -composite "
    trans << "\\( +clone -flop \\) -compose Multiply -composite "
    trans << "\\) +matte -compose CopyOpacity -composite "
  end

  def page()
    position = Picture.where("created_at >= ?", self.created_at).count
    (position.to_f/PER_PAGE).ceil
  end
end
