class AddMovieOggToMovie < ActiveRecord::Migration
  def up
    add_column :movies, :movie_ogg_file_name, :string
    add_column :movies, :movie_ogg_content_type, :string
    add_column :movies, :movie_ogg_file_size, :integer
  end

  def down
    remove_column :movies, :movie_ogg_file_name
    remove_column :movies, :movie_ogg_content_type
    remove_column :movies, :movie_ogg_file_size
  end
end
