class AddMovieToMovies < ActiveRecord::Migration
  def self.up
    add_column :movies, :movie_file_name, :string
    add_column :movies, :movie_content_type, :string
    add_column :movies, :movie_file_size, :integer
  end

  def self.down
    remove_column :movies, :movie_file_name
    remove_column :movies, :movie_content_type
    remove_column :movies, :movie_file_size
  end
end
